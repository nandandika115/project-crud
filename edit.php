<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="edit.css"> 
    <link rel="icon" href="images/fevicon/logo.png" type="image/png" />
    <title>Form Edit Data</title>
</head>
<body>
<?php
include ('koneksi.php');
//MENGAMBIL DATA PADA URL DENGAN KEY ID
$id = $_GET['id'];

//MENGAMBIL DATA BERDASARKAN ID
$query = mysqli_query($connect, "SELECT * FROM karyawan WHERE id = $id") or die (mysqli_error($connect));
$data = mysqli_fetch_array($query);

$role = $data['roles'];
$status = $data['status']

?>
    <form action="prosesEdit.php?id=<?php echo $data['id'] ?>" method="POST">
    <h1>EDIT DATA</h5>
    Username:
    <input type="text" placeholder="username" name="username" value="<?php echo $data['username'] ?>"  required><br>
    Nama:
    <input type="text" placeholder="nama" name="nama" value="<?php echo $data['nama']?>" required><br>
    <label>Password:
    <input type="password" placeholder="password" name="password" id="password" value="" onkeyup='check();' /><br>
    </label>
    <label>Confirm Password: 
    <input type="password" placeholder="confirm password" name="password" id="confirm_password" onkeyup='check();' />
    <span id='message'></span>
    </label>
    &nbsp; &nbsp; &nbsp; &nbsp;
    Roles :
    <select name="roles" >
        <option value="">Option</option>
        <option value="Admin" <?php if($role == 'Admin'){echo "selected";} ?>>Admin</option>
        <option value="Kasir" <?php if($role == 'Kasir'){echo "selected";} ?>>Kasir</option>
    </select><br>
    <h5>Status :</h5>
        <input name="status" value="aktif" type="radio" id="Aktif_radio"<?php if($status == 'aktif'){echo "checked";} ?>><label for="Aktif_radio">Aktif<br>
        <input name="status" value="Nonaktif" type="radio" id="Nonaktif_radio"<?php if($status == 'Nonaktif'){echo "checked";}?>><label for="Nonaktif_radio">Nonaktif<br>
    <input type="submit" value="Simpan"> <input type="reset" value="Reset"> <button><a href="contact.php">Batal</a>
 </form>
 
 <script src="input.js">

</script>

</body>
</html>