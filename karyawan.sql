-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 07, 2022 at 01:50 AM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 8.0.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pegawai`
--

-- --------------------------------------------------------

--
-- Table structure for table `karyawan`
--

CREATE TABLE `karyawan` (
  `id` int(10) NOT NULL,
  `username` varchar(20) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `roles` varchar(10) NOT NULL,
  `status` varchar(10) NOT NULL,
  `password` varchar(35) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `karyawan`
--

INSERT INTO `karyawan` (`id`, `username`, `nama`, `roles`, `status`, `password`) VALUES
(1, '50', 'Andika', 'Admin', 'aktif', '8977ecbb8cb82d77fb091c7a7f186163'),
(2, '7755', 'gung pedia', 'Admin', 'aktif', 'd41d8cd98f00b204e9800998ecf8427e'),
(3, '23', 'Arya', 'Admin', 'aktif', 'd41d8cd98f00b204e9800998ecf8427e'),
(13, '12', 'Alit', 'Admin', 'Nonaktif', ''),
(14, '34', 'Alfin', 'Kasir', 'aktif', ''),
(15, '20', 'DK', 'Admin', 'Nonaktif', ''),
(16, '6', 'Gung Alit', 'Admin', 'aktif', ''),
(19, '45', 'Ananta', 'Kasir', 'aktif', ''),
(24, '44', 'Andika', 'Kasir', 'Nonaktif', ''),
(27, '55', 'Alit', 'Admin', 'aktif', ''),
(35, '43', 'Yoga', 'Kasir', 'aktif', ''),
(36, '2345', 'Dika', 'Admin', 'aktif', '81b073de9370ea873f548e31b8adc081'),
(39, '3435', 'alit pedia', 'Admin', 'aktif', '827ccb0eea8a706c4c34a16891f84e7b'),
(40, '2244', 'Andika', 'Kasir', 'aktif', '3147da8ab4a0437c15ef51a5cc7f2dc4'),
(41, '7890', 'Arya Wijaya', 'Admin', 'aktif', '5882985c8b1e2dce2763072d56a1d6e5');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `karyawan`
--
ALTER TABLE `karyawan`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `karyawan`
--
ALTER TABLE `karyawan`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
