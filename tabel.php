<!DOCTYPE html>
<html lang="en">
<head>
	<link rel="stylesheet" href=".css">
	<title>Document</title>
</head>
<body>
	
</body>
</html>
<?php
include ('koneksi.php');
$batas = 10;
$halaman = isset($_GET['halaman'])?(int)$_GET['halaman'] : 1;
$halaman_awal = ($halaman>1) ? ($halaman * $batas) - $batas : 0;	
 
$previous = $halaman - 1;
$next = $halaman + 1;

$sql = 'SELECT *
		FROM karyawan';
		
$query = mysqli_query($connect, $sql);


echo '
	<div class="table-responsive">
	<table border="1" id="example" class="table table-striped table-bordered" style="width:100%">
		<thead>
			<tr>
                <th>No</th>
				<th>username</th>
				<th>nama</th>
				<th>roles</th>
				<th>status</th>
				<th>action</th>
			</tr>
		</thead>
		<tbody>';
$x = 1;		
while ($row = mysqli_fetch_array($query))
{
	echo '<tr>
            <td>'.$x.'</td>
			<td>'.$row['username'].'</td>
			<td>'.$row['nama'].'</td>
			<td class="right">'.$row['roles'].'</td>
			<td>'.$row['status'].'</td>
			<td><center><a href="edit.php?id='.$row['id'].'"><button class="btn btn-sm btn-warning ">Edit</button></a></center></td>
			
		</tr>';
    $x++;
}
echo '
	</tbody>
</table>
</div>';
